package Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Person implements Comparable<Person> {
    private String name;
    private String surname;
    private Integer age;

    public Person() {
    }

    public Person(String newName, String newSurname, Integer newAge) {
        name = newName;
        surname = newSurname;
        age = newAge;

    }

    public String getName() { return this.name; }
    public String getSurname() { return this.surname; }
    public Integer getAge() { return this.age; }

    @Override
    public int compareTo(Person otherPerson) {
        return Comparator.comparing(Person::getName)
                .thenComparing(Person::getSurname)
                .thenComparing(Person::getAge)
                .compare(this, otherPerson);
    }

    public void runLab () {
        List<Person> testList = new ArrayList();
        testList.add(new Person("b", "b", 10));
        testList.add(new Person("b", "b", 20));
        testList.add(new Person("a", "b", 30));
        testList.add(new Person("b", "a", 40));
        testList.add(new Person("a", "a", 20));
        testList.add(new Person("c", "a", 10));
        testList.add(new Person("b", "c", 50));
        testList.add(new Person("a", "b", 30));

        for(Person p : testList) {
            System.out.println("Name: " + p.getName() + ", Surname: " + p.getSurname() + ", Age: " + p.getAge());
        }

        Collections.sort(testList);
        System.out.println("Sorted ------------------------------------------------------------------------");

        for(Person p : testList) {
            System.out.println("Name: " + p.getName() + ", Surname: " + p.getSurname() + ", Age: " + p.getAge());
        }
    }
}
